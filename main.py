import numpy as np
import numpy.linalg as la
import matplotlib.pyplot as pp
import matplotlib.widgets as wig
import matplotlib.patches as pat
import lib.plotdefs as pd
import timeit
from pypylon import pylon
import os.path as path

pp.rcParams["keymap.back"] = []
pp.rcParams["keymap.copy"] = []
pp.rcParams["keymap.forward"] = []
pp.rcParams["keymap.fullscreen"] = []
pp.rcParams["keymap.grid"] = []
pp.rcParams["keymap.grid_minor"] = []
pp.rcParams["keymap.help"] = []
pp.rcParams["keymap.home"] = []
pp.rcParams["keymap.pan"] = []
pp.rcParams["keymap.quit"] = []
pp.rcParams["keymap.quit_all"] = []
pp.rcParams["keymap.save"] = []
pp.rcParams["keymap.xscale"] = []
pp.rcParams["keymap.yscale"] = []
pp.rcParams["keymap.zoom"] = []

def _gen_text_box(P: pd.Plotter, pos: list[float], initval: float, label: str):
    ax = P.fig.add_axes(pos)
    box = wig.TextBox(ax, label, initval, color="white", hovercolor="#e0e0e0",
        label_pad=0.08)
    return box

def _gen_button(P: pd.Plotter, pos: list[float], label: str):
    ax = P.fig.add_axes(pos)
    button = wig.Button(ax, label, color="white", hovercolor="#e0e0e0")
    return button

def _gen_window():
    P = pd.Plotter.new(nrows=2, figsize=[3.2, 1.8],
        gridspec_kw=dict(height_ratios=[2, 1]))
    P.fig.subplots_adjust(left=0.38, right=0.78, bottom=0.18, top=0.98)
    P[0].ggrid(False)
    P[1].ggrid(True)
    P[1] \
        .set_xlabel("Time [s]", labelpad=0.5) \
        .set_ylabel("$K \\int_A I(x, y) dA$", labelpad=0.5)
    boxes = {
        "exposure": _gen_text_box(P, [0.12, 0.81, 0.15, 0.04], 10 * 1000,
            "Exposure [$\\mu$s]"),
        "gain": _gen_text_box(P, [0.12, 0.76, 0.15, 0.04], 0.0,
            "Gain [dB]"),
        "i0": _gen_text_box(P, [0.12, 0.71, 0.06, 0.04], 0, "$i_0$"),
        "i1": _gen_text_box(P, [0.21, 0.71, 0.06, 0.04], 1, "$i_1$"),
        "j0": _gen_text_box(P, [0.12, 0.66, 0.06, 0.04], 0, "$j_0$"),
        "j1": _gen_text_box(P, [0.21, 0.66, 0.06, 0.04], 1, "$j_1$"),
        "dA": _gen_text_box(P, [0.12, 0.61, 0.06, 0.04], 1.0, "$dA$"),
        "K": _gen_text_box(P, [0.21, 0.61, 0.06, 0.04], 1.0, "$K$"),
        "Trec": _gen_text_box(P, [0.12, 0.34, 0.15, 0.04], 1.0,
            "$T_{rec}$ [s]"),
        "filename": _gen_text_box(P, [0.12, 0.03, 0.56, 0.04], "", "Filename"),
    }
    buttons = {
        "fit": _gen_button(P, [0.12, 0.54, 0.15, 0.04], "Fit Gaussian"),
        "record": _gen_button(P, [0.12, 0.29, 0.15, 0.04], "Record"),
        "save": _gen_button(P, [0.68, 0.03, 0.10, 0.04], "Save"),
        "quit": _gen_button(P, [0.145, 0.15, 0.10, 0.08], "Quit"),
    }
    R = P[0].ax.add_patch(
        pat.Rectangle((0, 0), 1, 1,
            edgecolor="C0", facecolor="none"
        )
    )
    P.fig.text(0.9, 0.75, "$K \\int_A I(x, y) dA$\n$=$", fontsize="large",
        horizontalalignment="center", verticalalignment="bottom")
    P.fig.text(0.85, 0.65, "$\\sigma_x =$", fontsize="large",
        horizontalalignment="right", verticalalignment="center")
    P.fig.text(0.85, 0.60, "$\\sigma_y =$", fontsize="large",
        horizontalalignment="right", verticalalignment="center")
    #P.fig.text(0.9, 0.28, "$T_{load}$ [s]\n$=$", fontsize="large",
    #    horizontalalignment="center", verticalalignment="bottom")
    texts = {
        "I": P.fig.text(0.9, 0.74, "0.00000e+00", fontsize="xx-large",
            horizontalalignment="center", verticalalignment="top"),
        "sx": P.fig.text(0.86, 0.65, "0.00000e+00", fontsize="large",
            horizontalalignment="left", verticalalignment="center"),
        "sy": P.fig.text(0.86, 0.60, "0.00000e+00", fontsize="large",
            horizontalalignment="left", verticalalignment="center"),
        #"Tload": P.fig.text(0.9, 0.27, "0.00000e+00", fontsize="large",
        #    horizontalalignment="center", verticalalignment="top"),
    }
    return P, boxes, buttons, texts, R

def modify_rectangle(R: pat.Rectangle, x0, x1, y0, y1):
    R.set_x(x0)
    R.set_y(y0)
    R.set_width(x1 - x0)
    R.set_height(y1 - y0)

def connect_camera():
    camera = pylon.InstantCamera(
        pylon.TlFactory.GetInstance().CreateFirstDevice()
    )
    camera.Open()
    camera.PixelFormat.SetValue("Mono12")
    camera.ExposureTime.SetValue(10000.0)
    camera.Gain.SetValue(0.0)
    camera.AcquisitionMode.SetValue("Continuous")
    camera.StartGrabbing(pylon.GrabStrategy_LatestImageOnly)
    return camera

def acquire_data(camera):
    res = camera.RetrieveResult(5000, pylon.TimeoutHandling_ThrowException)
    img = res.Array
    w, h = res.Width, res.Height
    t = res.GetTimeStamp() / 1e9
    res.Release()
    return img, w, h, t

def acquire_time_series(camera, Trec):
    global i0, i1, j0, j1, dA, K
    T = list()
    I = list()
    _, _, _, t0 = acquire_data(camera)
    Tmax = t0 + Trec
    t = 0
    while t < Tmax:
        X, _, _, t = acquire_data(camera)
        T.append(t)
        I.append(X)
    T = (np.array(T) - min(T))
    I = np.array([K * integrate_area(x[i0:i1, j0:j1] / 4095, dA) for x in I])
    return T, I

def rescale_display_data(X):
    return np.log10(1 + X[::20, ::20].astype(np.float64) / 4095 * 9)

def integrate_area(A, dA):
    return np.sum(A) * dA

def fit_gaussian(A, dA):
    imax, jmax = np.where(A == A.max())
    i0, j0 = imax.mean(), jmax.mean()
    x = np.sqrt(dA) * (np.arange(A.shape[1]) - j0)
    y = np.sqrt(dA) * (np.arange(A.shape[0]) - i0)
    X, Y = np.meshgrid(x, y)
    Z = (-2 * np.log(A / A.max())).flatten()
    M = np.array([X.flatten()**2, Y.flatten()**2]).T
    Sx, Sy = la.solve(M.T @ M, M.T @ Z)
    return np.sqrt(1 / Sx), np.sqrt(1 / Sy)

def main():
    global camera
    camera = connect_camera()
    global exposure_m, gain_m
    exposure_m = [camera.ExposureTime.Min, camera.ExposureTime.Max]
    gain_m = [camera.Gain.Min, camera.Gain.Max]

    global do_main_loop
    do_main_loop = True

    global P, boxes, buttons, texts, R
    P, boxes, buttons, texts, R = _gen_window()
    
    global X, w, h, t
    X, w, h, t = acquire_data(camera)
    im = P[0].imshow(rescale_display_data(X),
        cmap=pd.colormaps["plasma"], extent=[0, w, h, 0],
        log=False, chain=False)
    im.set_clim(0, 1)

    global time_data, time_fit
    time_data = P[1].plot([0], [0], "oC0", markersize=1.5,
        log=False, chain=False)[0]
    time_fit = P[1].plot([-0.5, 0.5], [0, 0], "--k", log=False, chain=False)[0]

    global i0, i1, j0, j1, dA, K, exposure, gain
    (i0, i1, j0, j1, exposure, gain) = (0, 1, 0, 1, 10000.0, 0.0)

    def set_i0(val):
        global boxes, R
        global i0, i1, j0, j1
        boxes["i0"].set_val(
            min(max(int(float(val)) if len(val) > 0 else i1 - 1, 0), i1 - 1)
        )
        modify_rectangle(R, j0, j1, i0, i1)
    boxes["i0"].on_submit(set_i0)
    boxes["i0"].on_text_change(lambda *args: None)

    def set_i1(val):
        global boxes, R, h
        global i0, i1, j0, j1
        boxes["i1"].set_val(
            min(max(int(float(val)) if len(val) > 0 else i0 + 1, i0 + 1), h)
        )
        modify_rectangle(R, j0, j1, i0, i1)
    boxes["i1"].on_submit(set_i1)
    boxes["i1"].on_text_change(lambda *args: None)

    def set_j0(val):
        global boxes, R
        global i0, i1, j0, j1
        boxes["j0"].set_val(
            min(max(int(float(val)) if len(val) > 0 else j1 - 1, 0), j1 - 1)
        )
        modify_rectangle(R, j0, j1, i0, i1)
    boxes["j0"].on_submit(set_j0)
    boxes["j0"].on_text_change(lambda *args: None)

    def set_j1(val):
        global boxes, R, w
        global i0, i1, j0, j1
        boxes["j1"].set_val(
            min(max(int(float(val)) if len(val) > 0 else j0 + 1, j0 + 1), w)
        )
        modify_rectangle(R, j0, j1, i0, i1)
    boxes["j1"].on_submit(set_j1)
    boxes["j1"].on_text_change(lambda *args: None)

    def set_exposure(val):
        camera.ExposureTime.SetValue(
            min(max(float(val), exposure_m[0]), exposure_m[1])
        )
        boxes["exposure"].set_val(camera.ExposureTime.GetValue())
    boxes["exposure"].on_submit(set_exposure)
    boxes["exposure"].on_text_change(lambda *args: None)

    def set_gain(val):
        camera.Gain.SetValue(
            min(max(float(val), gain_m[0]), gain_m[1])
        )
        boxes["gain"].set_val(str(camera.Gain.GetValue())[:5])
    boxes["gain"].on_submit(set_gain)
    boxes["gain"].on_text_change(lambda *args: None)

    def set_dA(val):
        global boxes
        boxes["dA"].set_val(float(val))
    boxes["dA"].on_submit(set_dA)
    boxes["dA"].on_text_change(lambda *args: None)

    def set_K(val):
        global boxes
        boxes["K"].set_val(float(val))
    boxes["K"].on_submit(set_K)
    boxes["K"].on_text_change(lambda *args: None)

    def set_Trec(val):
        global boxes
        boxes["Trec"].set_val(float(val))
    boxes["Trec"].on_submit(set_Trec)
    boxes["Trec"].on_text_change(lambda *args: None)

    def set_filename(val):
        global boxes
        boxes["filename"].set_val(val)
    boxes["filename"].on_submit(set_filename)
    boxes["filename"].on_text_change(lambda *arge: None)

    def do_fit(mouse_event):
        global texts, camera, dA, i0, i1, j0, j1
        X, _, _, _ = acquire_data(camera)
        sx, sy = fit_gaussian(X[i0:i1, j0:j1], dA)
        texts["sx"].set_text("{:.5e}".format(sx))
        texts["sy"].set_text("{:.5e}".format(sy))
    buttons["fit"].on_clicked(do_fit)

    def save_data(mouse_event):
        global boxes, camera
        res = camera.RetrieveResult(5000, pylon.TimeoutHandling_ThrowException)
        img = pylon.PylonImage()
        img.AttachGrabResultBuffer(res)
        root, ext = path.splitext(boxes["filename"].text)
        if ext in {".png", ".PNG"}:
            img.Save(pylon.ImageFileFormat_Png, boxes["filename"].text)
        elif ext in {".bmp", ".BMP"}:
            img.Save(pylon.ImageFileFormat_Bmp, boxes["filename"].text)
        elif ext in {".jpg", ".JPG", ".jpeg", ".JPEG"}:
            ipo = pylon.ImagePersistenceOptions()
            ipo.SetQuality(90)
            img.Save(pylon.ImageFileFormat_Jpeg, boxes["filename"].text, ipo)
        elif ext in {".npy", ".NPY"}:
            np.save(boxes["filename"].text, res.Array)
        else:
            np.savetxt(boxes["filename"].text, res.Array)
        img.Release()
        res.Release()
    buttons["save"].on_clicked(save_data)

    def do_record(mouse_event):
        global boxes, camera, time_data, time_fit, P
        global i0, i1, j0, j1, dA, K
        T, I = acquire_time_series(
            camera, float(boxes["Trec"].text), (i0, i1, j0, j1), dA, K)
        time_data.set_xdata(T)
        time_data.set_ydata(I)
        time_fit.set_xdata(T)
        time_fit.set_ydata(
            ((1 - 1 / np.e) * (I.max() - I.min()) + I.min())
            * np.ones(T.shape[0])
        )
        P[1].ax.set_xlim(
            T.min() - (T.mean() - T.min()) / 10,
            T.max() + (T.max() - T.mean()) / 10
        )
        P[1].ax.set_ylim(
            I.min() - (I.mean() - I.min()) / 10,
            I.max() + (I.max() - I.mean()) / 10
        )
    buttons["record"].on_clicked(do_record)

    def quit(mouse_event):
        global do_main_loop
        do_main_loop = False
    buttons["quit"].on_clicked(quit)

    pp.show(block=False)
    while do_main_loop:
        i0 = int(float(boxes["i0"].text)) if len(boxes["i0"].text) > 0 else i0
        i1 = int(float(boxes["i1"].text)) if len(boxes["i1"].text) > 0 else i1
        j0 = int(float(boxes["j0"].text)) if len(boxes["j0"].text) > 0 else j0
        j1 = int(float(boxes["j1"].text)) if len(boxes["j1"].text) > 0 else j1
        dA = int(float(boxes["dA"].text)) if len(boxes["dA"].text) > 0 else dA
        K = int(float(boxes["K"].text)) if len(boxes["K"].text) > 0 else K
        X, w, h, t = acquire_data(camera)
        im.set_data(rescale_display_data(X))
        texts["I"].set_text(
            "{:.5e}".format(K * integrate_area(X[i0:i1, j0:j1] / 4095, dA)))
        pp.pause(0.001)

    camera.StopGrabbing()
    camera.Close()

if __name__ == "__main__":
    main()

